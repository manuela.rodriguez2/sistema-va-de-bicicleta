package bsi.pm.vadebicicleta;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import bsi.pm.vadebicicleta.model.Bicicleta;
import bsi.pm.vadebicicleta.service.EquipamentoService;
import bsi.pm.vadebicicleta.utils.Status;

@SpringBootTest
class VaDeBicicletaApplicationTests {

	@Autowired
	private EquipamentoService equipamentoService;
	
	@Test
	void retornarBicicletasTest() {
		List<Bicicleta> resultadoEsperado = Arrays.asList(new Bicicleta("Caloi", "400 Comfort M", 
																		"2022", 1, Status.NOVA));
		
		List<Bicicleta> resultadoObtido = equipamentoService.retornarBicicletas();
		assertTrue(resultadoEsperado.size() == resultadoObtido.size() && resultadoEsperado.containsAll(resultadoObtido) && resultadoObtido.containsAll(resultadoEsperado));
	}
	
	@Test
	void retornaBicicletaByIdNullTest() {
		assertEquals(null, this.equipamentoService.retornarBicicletaById(null));
	}
	
	@Test
	void retornaBicicletaByIdTest() {
		assertEquals(new Bicicleta("Caloi", "400 Comfort M", 
				"2022", 1, Status.NOVA), this.equipamentoService.retornarBicicletaById("1"));
	}
	
}

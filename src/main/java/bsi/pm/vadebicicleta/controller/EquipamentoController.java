package bsi.pm.vadebicicleta.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bsi.pm.vadebicicleta.model.Bicicleta;
import bsi.pm.vadebicicleta.service.EquipamentoService;

@RestController
@RequestMapping("/equipamento")
public class EquipamentoController {

	@Autowired
	private EquipamentoService equipamentoService;
	
	@GetMapping("/bicicleta")
	public ResponseEntity<List<Bicicleta>> getBicicletas() {
		List<Bicicleta> bicicletas = this.equipamentoService.retornarBicicletas();

		return ResponseEntity.status(HttpStatus.OK).body(bicicletas);
	}
	
	@GetMapping("/bicicleta/{idBicicleta}")
	public ResponseEntity<Bicicleta> getBicicletasById(@PathVariable String idBicicleta) {
		Bicicleta bicicleta = this.equipamentoService.retornarBicicletaById(idBicicleta);
		if(Objects.isNull(bicicleta))
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		return ResponseEntity.status(HttpStatus.OK).body(bicicleta);
	}
}

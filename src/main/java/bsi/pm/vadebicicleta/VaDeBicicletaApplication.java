package bsi.pm.vadebicicleta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaDeBicicletaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaDeBicicletaApplication.class, args); 
	}

}

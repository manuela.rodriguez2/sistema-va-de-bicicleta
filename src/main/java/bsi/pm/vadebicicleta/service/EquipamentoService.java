package bsi.pm.vadebicicleta.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.common.base.Strings;

import bsi.pm.vadebicicleta.model.Bicicleta;
import bsi.pm.vadebicicleta.utils.Status;

@Service
public class EquipamentoService {

	public List<Bicicleta> retornarBicicletas() {
		return Arrays.asList(new Bicicleta("Caloi", "400 Comfort M", "2022",
										   1, Status.NOVA));
	}

	public Bicicleta retornarBicicletaById(String idBicicleta) {
		if(Strings.isNullOrEmpty(idBicicleta)) {
			return null;
		}
		return new Bicicleta("Caloi", "400 Comfort M", "2022",
				   1, Status.NOVA);
	}
}

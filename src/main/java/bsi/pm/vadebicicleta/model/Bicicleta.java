package bsi.pm.vadebicicleta.model;

import bsi.pm.vadebicicleta.utils.Status;

public class Bicicleta {

	private int id;
	private String marca;
	private String modelo;
	private String ano;
	private int numero;
	private Status status;
	
	public Bicicleta(String marca, String modelo, String ano,
					 int numero, Status status) {
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.numero = numero;
		this.status = status;
	}

	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object o) {
	    if (o == this)
	        return true;
	    if (!(o instanceof Bicicleta))
	        return false;
	    Bicicleta other = (Bicicleta) o;
	    return this.numero == other.numero && this.status == other.status &&
	    		this.marca == other.marca && this.ano == other.ano && 
	    		this.modelo == other.modelo;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}